from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

REQUIRED = [
    'opencv_python',
    'numpy',
    'hein_utilities',
    'heinsight',
    'newera',
]

DEPENDENCY_LINKS = [
    'https://gitlab.com/heingroup/heinsight.git#egg=master',
    'https://gitlab.com/heingroup/newera.git#egg=master',
]

setup(
    name='liquid_level_workflows',
    version='1.1.1',
    packages=find_packages(),
    url='https://gitlab.com/heingroup/liquid_level_workflows',
    license='MIT',
    author='Veronica Lai, Tara Zepel // Hein Group',
    author_email='',
    description='Automation workflows that are based on using computer vision and liquid level detection to control the workflow that use heinsight',
    long_description=long_description,
    long_description_content_type='text/markdown',
    install_requires=REQUIRED,
    dependency_links=DEPENDENCY_LINKS
)

