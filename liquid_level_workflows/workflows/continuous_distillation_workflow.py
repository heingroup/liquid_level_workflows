import time
from datetime import datetime
from heinsight.vision.liquid_level import LiquidLevel, NoMeniscusFound
from heinsight.heinsight_utilities.time_manager import TimeManager
from heinsight.heinsight_utilities.try_tracker import TryTracker
from liquid_level_workflows.workflow_utilities.liquid_level_control import calculate_variable_pump_time_new_era_peristaltic_pump
from hein_utilities.slack_integration.slack_managers import RTMControlManager
from liquid_level_workflows.workflows.generic_workflow import LiquidLevelGenericWorkFlow


class ContinuousDistillationWorkflow(LiquidLevelGenericWorkFlow):
    def __init__(self,
                 liquid_level: LiquidLevel,
                 try_tracker: TryTracker,
                 time_manager: TimeManager,
                 pump,
                 do_variable_self_correction: bool,
                 number_of_monitor_liquid_level_replicate_measurements: int = 3,
                 advance_time: int = 15,
                 slack_bot: RTMControlManager = None,
                 save_folder_bool: bool = True,
                 save_folder_name: str = None,
                 save_folder_location: str = None,
                 wait_time: int = 1,
                 ):
        """
        :param liquid_level:
        :param try_tracker:
        :param time_manager:
        :param pump:
        :param do_variable_self_correction:
        :param int, advance_time: the number of seconds to allow filtration to occur before checking the liquid level
        :param slack_bot:
        :param save_folder_bool:
        :param save_folder_name:
        :param save_folder_location:
        :param wait_time:
        """
        super().__init__(liquid_level=liquid_level,
                         try_tracker=try_tracker,
                         time_manager=time_manager,
                         do_variable_self_correction=do_variable_self_correction,
                         number_of_monitor_liquid_level_replicate_measurements
                         =number_of_monitor_liquid_level_replicate_measurements,
                         slack_bot=slack_bot,
                         save_folder_bool=save_folder_bool,
                         save_folder_name=save_folder_name,
                         save_folder_location=save_folder_location
                         )
        self.wait_time = wait_time
        self.pump = pump
        self.advance_time = advance_time
        self.direction_to_withdraw = 'withdraw'
        self.direction_to_dispense = 'dispense'

    def pre_run(self,
                image=None,
                select: bool = True,
                ):
        """
        see superclass

        :param image:
        :param select:
        :return:
        """
        # next line can throw NoMeniscusFound exception
        if image is None:
            image = self.liquid_level_monitor.liquid_level.camera.take_picture()

        curr_time = self.datetime_manager.now_string()
        self.raw_images_folder.save_image_to_folder(image_name=curr_time,
                                                    image=image)

        if select is True:
            if self.do_variable_self_correction is True:
                self.liquid_level_monitor.liquid_level.start(
                    image=image,
                    select_region_of_interest=True,
                    set_reference=True,
                    select_tolerance=True,
                )
            else:
                self.liquid_level_monitor.liquid_level.track_liquid_tolerance_levels.reference_row = 0
                self.liquid_level_monitor.liquid_level.start(
                    image=image,
                    select_region_of_interest=True,
                    set_reference=False,
                    select_tolerance=True,
                )
        else:
            self.liquid_level_monitor.liquid_level.start(
                image=image,
                select_region_of_interest=False,
                set_reference=False,
                select_tolerance=False,
            )
        print(f'pre_run complete')

    def run(self,
            image=None,
            do_pre_run: bool = True,
            select: bool = True,
            ):
        super().run(image=image,
                    do_pre_run=do_pre_run,
                    select=select)

    def set_advance_time(self,
                         advance_time: int):
        self.advance_time = advance_time

    def advance(self):
        """
        wait for a certain period of time before monitoring the liquid level to know if more liquid needs to be added
        in to maintain the level
        :return:
        """
        time.sleep(self.advance_time)

    def end_sequence(self,
                     time_since_started: float):
        """
        ending sequence of things to do. post slack messages to let user know the end time has been reached

        :param: float, time_since_started: number of hours that have elapsed since start time
        :return:
        """
        super().end_sequence(time_since_started=time_since_started)

    def monitor_liquid_level(self, list_of_percent_diff):
        return super().monitor_liquid_level(list_of_percent_diff=list_of_percent_diff)

    def not_in_tolerance(self, percent_diff):
        return super().not_in_tolerance(percent_diff=percent_diff)

    def self_correct(self,
                     percent_diff: float,
                     ):
        """
        Self-correct according to if there is too little liquid in the vial relative to the reference line by adding
        more in

        :param float, percent_diff: percentage relative to height of the image of the distance between the most
           recently measured meniscus line and the reference meniscus line
        :return:
        """
        raise NotImplementedError

    def calculate_how_much_to_self_correct(self,
                                           percent_diff: float,
                                           ):
        raise NotImplementedError

    def variable_self_correct(self,
                           percent_diff: float,
                           ):
        """
        variablely self-correct according to if there is too little liquid in the vial relative to the reference
        line by pumping - the pumping is guided by vision. Need to pump according to the difference between the
        reference and current liquid level, but only pump 80% of what is needed to go back to the reference meniscus
        level is actually pumped; this is for just in case an incorrect meniscus was found, so that self correction is
        more robust

        :param float, percent_diff: percentage relative to height of the image of the distance between the most
           recently measured meniscus line and the reference meniscus line. if it is a negative number then the
           current liquid level is below the reference liquid level.
        :return:
        """
        return super().variable_self_correct(percent_diff=percent_diff)

    def pump_self_correct(self,
                          time_to_pump,
                          direction,
                          ):
        raise NotImplementedError

    def calculate_how_much_to_variable_self_correct(self,
                                                 percent_diff: float,
                                                 ):
        raise NotImplementedError

    def check_if_should_try_again(self,
                                  no_meniscus_error: NoMeniscusFound,
                                  ):
        super().check_if_should_try_again(no_meniscus_error=no_meniscus_error)

    def try_again(self,
                  no_meniscus_error: NoMeniscusFound,
                  ):
        """
        Do what the superclass does but also save images of what the error looks like to disk
        :param no_meniscus_error:
        :return:
        """
        super().try_again(no_meniscus_error=no_meniscus_error)

    def do_not_try_again(self,
                         no_meniscus_error: NoMeniscusFound,
                         ):
        """
        Do what the superclass does but also save images of what the error looks like to disk and send slack messages
        of the error images.

        :param no_meniscus_error:
        :return:
        """
        super().do_not_try_again(no_meniscus_error=no_meniscus_error)

    def do_if_a_time_interval_has_passed(self,
                                         time: datetime):
        """
        Do this if a time interval has passed based on time passed through. add the new time interval to the time
        manager, and save the latest image analyzed to the folder for slack images to be saved in. then post that
        image to slack if there is a RTMControlManager

        :param datetime, time:
        :return:
        """
        super().do_if_a_time_interval_has_passed(time=time)

    def save_last_drawn_image(self):
        """
        Save the last image with drawn lines of where the liquid levels are to the computer

        :return: str, path_to_save_image: path to the image that was saved
        """
        return super().save_last_drawn_image()

    def post_slack_message(self,
                           message: str):
        super().post_slack_message(message=message)

    def post_slack_file(self,
                        file_path: str,
                        message: str,
                        ):
        super().post_slack_file(file_path=file_path,
                                message=message)

    def create_folder_hierarchy(self):
        super().create_folder_hierarchy()


class ContinuousDistillationPeristalticPump(ContinuousDistillationWorkflow):
    def __init__(self,
                 liquid_level: LiquidLevel,
                 try_tracker: TryTracker,
                 time_manager: TimeManager,
                 pump,
                 do_variable_self_correction: bool,
                 time_to_self_correct: int = 10,  # number of seconds to allow only 1 pump to run for liquid level
                 # self correction in the 'non-variable' way to correct the liquid level
                 number_of_monitor_liquid_level_replicate_measurements: int = 3,
                 advance_time: int = 15,
                 slack_bot: RTMControlManager = None,
                 save_folder_bool: bool = True,
                 save_folder_location: str = None,
                 save_folder_name: str = None,
                 wait_time: int = 1,
                 ):
        """
        Uses a New Era peristaltic pump

        :param liquid_level:
        :param try_tracker:
        :param time_manager:
        :param pump:
        :param do_variable_self_correction:
        :param slack_bot:
        :param save_folder_bool:
        :param save_folder_location:
        :param save_folder_name:
        :param wait_time:
        """
        super().__init__(liquid_level=liquid_level,
                         try_tracker=try_tracker,
                         time_manager=time_manager,
                         pump=pump,
                         do_variable_self_correction=do_variable_self_correction,
                         number_of_monitor_liquid_level_replicate_measurements
                         =number_of_monitor_liquid_level_replicate_measurements,
                         advance_time=advance_time,
                         slack_bot=slack_bot,
                         save_folder_bool=save_folder_bool,
                         save_folder_location=save_folder_location,
                         save_folder_name=save_folder_name,
                         wait_time=wait_time,
                         )
        self.time_to_pump_correction = time_to_self_correct  # hard coded in non-variable self correction value
        self.direction_to_withdraw = 'withdraw'
        self.direction_to_dispense = 'dispense'

    def pre_run(self,
                image=None,
                select: bool = True,
                ):
        super().pre_run(image=image,
                        select=select,
                        )

    def run(self,
            image=None,
            do_pre_run: bool = True,
            select: bool = True,
            ):
        super().run(image=image,
                    do_pre_run=do_pre_run,
                    select=select)

    def advance(self):
        super().advance()

    def self_correct(self,
                     percent_diff: float,
                     ):
        self_correct_time = self.calculate_how_much_to_self_correct(percent_diff=percent_diff)

        # since there will be 1 tolerance line, under which the liquid level is said to be out of bounds, this means
        # that the only direction you would need to self correct is to add more liquid in aka dispense
        self_correct_direction = self.direction_to_dispense

        print(f'direction to pump: {self_correct_direction}, '
              f'time to pump to self correct: f{self_correct_time}')
        self.post_slack_message(f'direction to pump: {self_correct_direction}, '
                                f'time to pump to self correct: {self.time_to_pump_correction} seconds')

        self.pump.pump(pump_time=self_correct_time,
                       direction=self_correct_direction,
                       wait_time=self.wait_time,
                       )

        if self.liquid_level_monitor.application_liquid_level_data_save_file_path is not None:
            self.liquid_level_monitor.update_json_file_with_new_liquid_level_correction_data_values()

    def calculate_how_much_to_self_correct(self,
                                           percent_diff: float,
                                           ):
        return self.time_to_pump_correction

    def variable_self_correct(self,
                           percent_diff: float,
                           ):
        """

        :param float percent_diff:  if it is a negative number then the
           current liquid level is below the reference liquid level.
        :return:
        """
        return super().variable_self_correct(percent_diff=percent_diff)

    def pump_self_correct(self,
                          time_to_pump,
                          direction,
                          ):
        self.pump.pump(pump_time=time_to_pump,
                       direction=direction,
                       wait_time=self.wait_time,
                       )

    def calculate_how_much_to_variable_self_correct(self,
                                                    percent_diff: float,
                                                    ):

        """
        Find out the time required in order to move the liquid level so that it moves to be 80% closer to the
        reference level

        # Calls the calculate_variable_pump_time_new_era_peristaltic_pump function from the liquid level package to
        calculate the time
        # required to
        # self correct the current liquid level to the reference liquid level, and find the rate to spin the
        # peristaltic
        # pump to achieve this

        :param float, percent_diff: percentage relative to height of the image of the distance between the most
           recently measured meniscus line and the reference meniscus line
        :return:
        """
        # need to remember that liquid level gives the ratio in terms of pixels, but for this,the percent_diff is
        # given as a float that is a percentage of the image total height. Since the same width and height of the
        # image should be used when comparing the current image to the first image that liquid level's
        # find_pump_to_pixel_ratio must have been run on, just need to convert between pixels and relative height of
        # the image

        absolute_value_of_percent_diff = abs(percent_diff) * 0.8  # need to use the absolute value when finding how many
        # pixels to correct by, since the percent_diff value can be a negative number if the change in meniscus
        # height is below the reference meniscus

        # to convert the relative percent_diff to number of pixels, need to multiply image height (from liquid level
        # instance) by the percent_diff
        image_height, image_width, _ = self.liquid_level_monitor.liquid_level.loaded_image.shape

        pixels_need_to_self_correct_by = int(image_height * absolute_value_of_percent_diff)

        time_to_self_correct, rpm_for_pump_to_pixel_ratio = \
            calculate_variable_pump_time_new_era_peristaltic_pump(
                liquid_level=self.liquid_level_monitor.liquid_level,
                pixels_need_to_self_correct_by=pixels_need_to_self_correct_by)

        return time_to_self_correct

