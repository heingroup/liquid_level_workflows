import os
import time
from datetime import datetime
from hein_utilities.slack_integration.slack_managers import RTMControlManager
from heinsight.heinsight_utilities.files import HeinsightFolder
from heinsight.heinsight_utilities.time_manager import TimeManager
from heinsight.heinsight_utilities.try_tracker import TryTracker
from heinsight.vision.liquid_level import LiquidLevel, NoMeniscusFound
from heinsight.vision.liquid_level import LiquidLevelMonitor
from hein_utilities.datetime_utilities import datetimeManager


class LiquidLevelGenericWorkFlow:
    def __init__(self,
                 liquid_level: LiquidLevel,
                 try_tracker: TryTracker,
                 time_manager: TimeManager,
                 number_of_monitor_liquid_level_replicate_measurements: int = 3,
                 slack_bot: RTMControlManager = None,
                 do_variable_self_correction: bool = False,
                 save_folder_bool: bool = False,
                 save_folder_name: str = None,
                 save_folder_location: str = None,
                 ):
        """
        :param LiquidLevel, liquid_level: instance of LiquidLevel
        :param TryTracker, try_tracker:
        :param TimeManager, time_manager:
        :param bool, do_variable_self_correction:
        :param int, number_of_monitor_liquid_level_replicate_measurements: must be an odd number, is the number of times to try
            to find a liquid level when monitoring the liquid level, the results of which will be averaged to a
            single measurements for where the liquid level is and if it in tolerance or not
        :param RTMControlManager, slack_bot:
        :param bool, save_folder_bool: True if at the end of the application you want to save all the images
            that were taken and used throughout the application run to use
        :param str, save_folder_name: Name of the save folder - generally it would be the experiment name
        :param str, save_folder_location: location of the main folder to save subfolders of the images taken during
            the experiment and the liquid level data json file from the experiment in. the save_folder_name should be
            included in this string (the last part of it)
        :param int, number_of_monitor_liquid_level_replicate_measurements: must be an odd number, is the number of times to try
            to find a liquid level when monitoring the liquid level, the results of which will be averaged to a
            single measurements for where the liquid level is and if it in tolerance or not
        """
        self.try_tracker = try_tracker
        self.time_manager = time_manager
        self.number_of_monitor_liquid_level_replicate_measurements = number_of_monitor_liquid_level_replicate_measurements
        self.do_variable_self_correction = do_variable_self_correction
        self.datetime_manager = datetimeManager('%Y_%m_%d_%H_%M_%S')

        self.save_folder_bool = save_folder_bool

        self.slack_bot = slack_bot
        self.run_experiment = True
        self.pause_experiment = False  # used pause the experiment

        # create initial main folder to save all images to
        if save_folder_name is None:
            save_folder_name = 'liquid_level_workflow_experiment'

        if save_folder_location is None:
            save_folder_location = os.path.join(os.path.abspath(os.path.curdir), save_folder_name)

        # folder object pointing to the
        self.save_folder = HeinsightFolder(
            folder_name=save_folder_name,
            folder_path=save_folder_location,
        )
        self.save_folder_name = save_folder_name

        # all these subfolders will be under the save_folder
        self.slack_images_folder: HeinsightFolder = None  # folder for all images that will be sent through slack
        self.all_drawn_images_folder: HeinsightFolder = None  # folder of all images from liquid level with lines
        # drawn on to
        # indicate where the liquid levels and tolerance levels and reference level are
        self.raw_images_folder: HeinsightFolder = None  # folder to same all raw non drawn on images

        self.create_folder_hierarchy()

        self.liquid_level_monitor = LiquidLevelMonitor(liquid_level=liquid_level,
                                                       application_liquid_level_data_save_folder_path=self.save_folder.path,
                                                       )

    def pre_run(self,
                image=None,
                select: bool = True,
                ):
        """
        Typically the first step for the run of an experiment. This has been singled out because there may be
        additional pre-run steps or optimizations that must be done before the application can be run, and so this
        pre-run step has been separated from the normal run step for ease of re-running an application and continuing
        to run an experiment without having to restart the whole script again.

        :param image, an image. This will be the image that will be used to select the reference and tolerance
            levels. If none, the camera in the liquid level attribute will be used to take a photo.
        :param bool, select: Whether you need to do any additional steps before the first step of the run or not.
            typically involves deciding to select a region of interest, a reference row, or tolerance levels, or not
        :return:
        """
        raise NotImplementedError

    def run(self,
            image=None,
            do_pre_run: bool = True,
            select=True,
            ):
        """
        The main function that puts together the methods to run liquid level monitoring of an image (that either must be
        passed through or else take a picture with the camera and use that), and then advances the system based on
        analysis of the image. What decision needs to be made based on the analysis needs to be separately
        implemented in each of the subclasses.

        The default action for the superclass run is only to delete the folder of images that were saved to the disk
        if the user doesn't want to save the images

        :param bool, do_pre_run: Whether to do the pre_run step or not
        :param image, an image. This will be the image that will be used to monitor the liquid level and go through
            the run. If none, the camera in the liquid level attribute will be used to take a photo.
        :param bool, select:
        :return:
        """
        try:
            if do_pre_run is True:
                try:
                    self.pre_run(image=None, select=select)
                except NoMeniscusFound as error:
                    self.check_if_should_try_again(no_meniscus_error=error)

            self.liquid_level_monitor.set_up_applications_liquid_level_data_save_file()

            # the overall loop
            while self.run_experiment is True:
                if self.pause_experiment is not True:
                    # send slack message every interval of time specified in init to remind user that CPC is still running
                    curr_time = datetime.now()
                    time_since_started = self.time_manager.time_since_started(time=curr_time)

                    has_a_time_interval_elapsed = self.time_manager.has_a_time_interval_elapsed(time=curr_time)
                    if has_a_time_interval_elapsed:
                        self.do_if_a_time_interval_has_passed(time=curr_time)

                    # check if experiment has run for the amount of time user specified or not, if it has or has gone
                    # over; if gone over then end the script
                    after_end_time = self.time_manager.is_after_end_time(time=curr_time)
                    if after_end_time:
                        self.end_sequence(time_since_started=time_since_started)
                        return

                    try:
                        list_of_percent_diff = self.take_multiple_photos_and_run_liquid_level()
                        tolerance_bool, percent_diff = self.liquid_level_monitor.monitor_liquid_level(list_of_percent_diff=list_of_percent_diff)
                        if self.try_tracker.get_try_counter() is not 0:
                            self.post_slack_message('Liquid level found. Reset try counter to 0')
                            self.try_tracker.reset_try_counter()

                            date_time_with_line, line_img = self.liquid_level_monitor.get_last_line_image()
                            line_image_path = self.slack_images_folder.save_image_to_folder(
                                image_name=f'reset_try_counter_{date_time_with_line}',
                                image=line_img,
                            )

                    except NoMeniscusFound as error:
                        self.check_if_should_try_again(error)
                        continue

                    if tolerance_bool:  # if the most recent measured meniscus level found is within the tolerance bounds
                        # advance normally next line can throw NoMeniscusFound exception
                        try:
                            self.advance()
                        except NoMeniscusFound as error:  # if algorithm couldnt find a meniscus
                            self.check_if_should_try_again(error)
                            continue
                    else:  # most recent measured meniscus was not within tolerance bounds
                        self.not_in_tolerance(percent_diff=percent_diff)
                else:  # self.pause_experiment is True
                    time.sleep(30)
            else:  # self.run_experiment is not True
                self.post_slack_message('experiment was manually ended')
                return

        except KeyboardInterrupt as error:
            self.post_slack_message('Stopped application script using Keyboard Interrupt')

        except Exception as error:
            print('Run has failed')
            print(f'Error: {error}')
            # write the last seen image before the error image, and send that to user through slack
            self.post_slack_message(f'Something went wrong with the run. Error encountered: {error} :cry:')

            date_time_with_line, line_img = self.liquid_level_monitor.get_last_line_image()
            date_time_for_edge, edge_img = self.liquid_level_monitor.get_last_edge_image()

            line_image_path = self.slack_images_folder.save_image_to_folder(
                image_name=f'failed_exit_run_image_{date_time_with_line}',
                image=line_img
            )
            edge_image_path = self.slack_images_folder.save_image_to_folder(
                image_name=f'failed_exit_run_image_edge_{date_time_for_edge}',
                image=edge_img
            )
            self.post_slack_file(line_image_path,
                                 'Last image before error')
            self.post_slack_file(edge_image_path,
                                 'Last image before error')

            raise error

        finally:
            if self.liquid_level_monitor.application_liquid_level_data_save_file_path is not None:
                self.liquid_level_monitor.update_json_file_with_new_liquid_level_data_values()

            if self.save_folder_bool is False:
                self.save_folder.delete_from_disk()

    def not_in_tolerance(self,
                         percent_diff: float,
                         ):
        """
        Do this if the liquid level was found not to be within tolerance
        :param: float, percent_diff, the relative distance of the current liquid level from a set reference level
        :return:
        """
        """
        What to do if the liquid level was not found to be within tolerance

        :param float, percent_diff: the relative distance of the current liquid level from a set reference level
        :return: 
        """
        date_time_with_line, line_img = self.liquid_level_monitor.get_last_line_image()

        line_image_path = self.slack_images_folder.save_image_to_folder(
            image_name=f'out_of_tolerance_{date_time_with_line}',
            image=line_img
        )

        if percent_diff > 0:
            above_or_below = 'above'
        else:  # if less than tolerance then pump into the vial being watched
            above_or_below = 'below'

        print(f'liquid level {above_or_below} tolerance')
        print(f'current liquid level difference from the reference liquid level by {percent_diff}')
        self.post_slack_message(f'Liquid level moved {above_or_below} the tolerance boundaries. '
                                f'Current liquid level differs from the reference liquid level by {percent_diff}')

        # self.post_slack_file(line_image_path,
        #                      'Liquid level out of tolerance bounds image')

        # next line can throw NoMeniscusFound exception
        if self.do_variable_self_correction is True:
            self.variable_self_correct(percent_diff=percent_diff)
        else:
            self.self_correct(percent_diff=percent_diff)

        # next line can throw NoMeniscusFound exception
        try:
            # immediately analyze a photo after self correction step and send message and picture to user so they can
            # tell immediately if self correction worked or not
            tolerance_bool, percent_diff, path_to_drawn_save_image = self.take_picture_and_run_liquid_level_algorithm()
            self.post_slack_message(f'After self correction - liquid level within tolerance bounds: {tolerance_bool}, '
                                    f'liquid level differs from the reference liquid level by {percent_diff}')
            # self.post_slack_file(path_to_drawn_save_image,
            #                      'Liquid level image after self correction')
            print(f'After self correction - liquid level within tolerance bounds: {tolerance_bool}, '
                  f'liquid level differs from the reference liquid level by {percent_diff}')
        except NoMeniscusFound as error:
            self.check_if_should_try_again(no_meniscus_error=error)

    def take_picture_and_run_liquid_level_algorithm(self):
        """
        Convenience method to take a photo and add that to the folder of raw saved images, and run the liquid level
        finding algorithm on that image and save an image of the image with the drawn levels on it. Then return the
        tolerance_bool (if the liquid level was within tolerance) and the percent_diff (percentage of full image
        height difference between the identified current liquid level and user set reference level). also return the
        path to the drawn image that was saved

        :return: tolerance_bool, bool: True if the liquid level is within tolerance. float, percent_diff: the
            relative distance the identified liquid level is away from the user set reference level.
            str, path_to_save_image
        """
        curr_time = self.datetime_manager.now_string()
        image = self.liquid_level_monitor.take_picture()
        self.raw_images_folder.save_image_to_folder(image_name=curr_time,
                                                    image=image)
        tolerance_bool, percent_diff = self.liquid_level_monitor.run_liquid_level_algorithm(image=image)

        path_to_drawn_save_image = self.save_last_drawn_image()
        return tolerance_bool, percent_diff, path_to_drawn_save_image

    def monitor_liquid_level(self, list_of_percent_diff):
        return self.liquid_level_monitor.monitor_liquid_level(list_of_percent_diff)

    def advance(self):
        """
        The main thing the application needs to do, either before or after it monitors the liquid level, depending on
        how the run method is implemented nd the order of this function and the monitor_liquid_level function inside
        of run().

        :return:
        """
        # this should take the place of cycle in the peristaltic one pump loop, and should be the action/whatever
        # happens in between when you need to use the camera again to monitor the liquid level
        raise NotImplementedError

    def end_sequence(self,
                     time_since_started: float) -> None:
        """
        do this if the run has ended by going to the end time based on the time manager
        :param: float, time_since_started: number of hours that have elapsed since start time
        :return:
        """
        self.post_slack_message(f'Automated slurry filtration has run for {round(time_since_started, 2)} hours, '
                                f'the end time was {self.time_manager.end_time} hours after starting at '
                                f'{self.time_manager.start_time}. The run has completed :tada:')

        date_time_with_line, line_img = self.liquid_level_monitor.liquid_level.all_images_with_lines[-1]
        line_image_path = self.slack_images_folder.save_image_to_folder(
            image_name=f'last_image_for_run',
            image=line_img
        )
        self.post_slack_file(
            line_image_path,
            'The last image taken for experiment')

    def take_multiple_photos_and_run_liquid_level(self):
        # instead of just taking one picture, take 3 and find the average of the tolerance_bool values and
        # round the value. Once rounded, then only take the corresponding percent_diff values that have the same
        # tolerance_bool value, where 0 is False and 1 is True, and average them to get the position of the liquid
        # level relative to how far from the reference level it is. this way, if the analysis of one image incorrectly
        # identifies the location of the liquid level compared to the other images, its value will be ignored
        list_of_percent_diff = []
        number_of_times_to_try = self.number_of_monitor_liquid_level_replicate_measurements  # must be an odd number
        while number_of_times_to_try > 0:
            # loop through the number of times to try to take pictures to try to find the liquid level. percent_diff is
            # how far away the meniscus is from the the reference liquid level line. collect these all into
            # list_of_percent_diff
            try:
                _, percent_diff, _ = self.take_picture_and_run_liquid_level_algorithm()
                self.try_tracker.reset_try_counter()
                list_of_percent_diff.append(percent_diff)
                number_of_times_to_try -= 1
            except NoMeniscusFound as error:
                self.check_if_should_try_again(no_meniscus_error=error)
        return list_of_percent_diff

    def self_correct(self,
                     percent_diff: float,
                     ):
        """
        Simple self correction step to do.

        :return:
        """
        raise NotImplementedError

    def calculate_how_much_to_self_correct(self,
                                           percent_diff: float,
                                           ):
        """
        Calculate how much to self correct by - used for variable self correct

        For the subclass implementations, a parameter can be passed through for the function.
        :param: float, percent_diff, the relative distance of the current liquid level from a set reference level
        :return:
        """
        raise NotImplementedError

    def variable_self_correct(self,
                           percent_diff: float,
                           ):
        """
        variable self correction step to do.

        :return:
        """
        raise NotImplementedError

    def pump_self_correct(self,
                          time_to_pump,
                          direction,
                          ):
        raise NotImplementedError

    def calculate_how_much_to_variable_self_correct(self,
                                                 percent_diff: float,
                                                 ):
        """
        Calculate how much to variable self correct by

        For the subclass implementations, a parameter can be passed through for the function.
        :return:
        """
        raise NotImplementedError

    # todo should rename this
    def check_if_should_try_again(self,
                                  no_meniscus_error: NoMeniscusFound,
                                  ):
        """
        If run into a NoMeniscusFound error, then need to run this to check if the application run should continue or
        end.

        More specifically: Check, in the try tracker, if the try counter is less than the maximum number of tries; the
        tries are tries to find the liquid level again after unsuccessfully finding it, if the try  counter is not
        within the maximum number of tries the stop the application run.

        For the subclass implementations, maybe also do something like record time the the maximum number of tries
        was hit and send a slack message or save the last image that caused the error.

        :param NoMeniscusFound, no_meniscus_error: The error thrown; should be a NoMeniscusError
        :return:
        """
        try_tracker_not_reached_maximum_number_of_tries = self.try_tracker.not_reached_maximum_number_of_tries()

        if try_tracker_not_reached_maximum_number_of_tries:
            self.try_again(no_meniscus_error=no_meniscus_error)
        else:
            self.do_not_try_again(no_meniscus_error=no_meniscus_error)

    def try_again(self,
                  no_meniscus_error: NoMeniscusFound,
                  ):
        """
        Do this if the try limit has not been reached yet. A more specific implementation may be implemented in
        the subclasses

        :param NoMeniscusFound, no_meniscus_error:
        :return:
        """
        current_try = self.try_tracker.get_try_counter()
        maximum_number_of_tries = self.try_tracker.get_max_number_of_tries()
        print(f'Could not find a liquid level. Maximum number of tries has not been reached yet - will try again. On '
              f'try {current_try} out of {maximum_number_of_tries}')
        self.post_slack_message(f'Could not find a liquid level. Maximum number of tries has not been reached yet - '
                                f'will try again. On try {current_try} out of {maximum_number_of_tries}')

        self.try_tracker.increment_try_counter()
        # writes the image where the error occurred to the main folder of images
        time = self.datetime_manager.now_string()
        error_image = no_meniscus_error.error_image
        error_contour_image = no_meniscus_error.contour_image

        error_image_path = self.slack_images_folder.save_image_to_folder(
            image_name=f'{time}_no_meniscus_found',
            image=error_image
        )
        error_contour_image_path = self.slack_images_folder.save_image_to_folder(
            image_name=f'{time}_no_meniscus_found_contour',
            image=error_contour_image
        )

    def do_not_try_again(self,
                         no_meniscus_error: NoMeniscusFound,
                         ):
        """
        Do this if the try limit has been reached . A more specific implementation may be implemented in the
        subclasses. Raise the error in order to end the application run

        :param NoMeniscusFound no_meniscus_error:
        :return:
        """
        maximum_number_of_tries = self.try_tracker.get_max_number_of_tries()
        print(f'Could not find a liquid level. Maximum number of {maximum_number_of_tries} tries has been reached - '
              f'application run will end')
        self.post_slack_message(f'Could not find a liquid level. Maximum number of {maximum_number_of_tries} tries '
                                f'has been reached - application run '
                                f'will end')

        time = self.datetime_manager.now_string()
        error_image = no_meniscus_error.error_image
        error_contour_image = no_meniscus_error.contour_image

        error_image_path = self.slack_images_folder.save_image_to_folder(
            image_name=f'{time}_no_meniscus_found',
            image=error_image
        )
        error_contour_image_path = self.slack_images_folder.save_image_to_folder(
            image_name=f'{time}_no_meniscus_found_contour',
            image=error_contour_image
        )

        self.post_slack_file(error_image_path, 'Error image')
        self.post_slack_file(error_contour_image_path, 'Error closed image')

        raise no_meniscus_error

    def do_if_a_time_interval_has_passed(self,
                                         time: datetime):
        hours_elapsed = self.time_manager.hours_elapsed(time=time)
        interval = self.time_manager.calculate_interval(hours_elapsed=hours_elapsed)
        self.time_manager.append_interval(interval=interval)

        time_since_started = self.time_manager.time_since_started(time=time)
        self.post_slack_message(message=f'application has been running for {round(time_since_started, 2)} hours and is '
                                        f'still running')

        if self.time_manager.more_than_one_interval_has_elapsed():  # if the experiment has been running
            # and its not the start of the run, then send an image to the user of what the last image
            # with lines looks like
            date_time_with_line, line_img = self.liquid_level_monitor.get_last_line_image()
            line_image_path = self.slack_images_folder.save_image_to_folder(
                image_name=f'interval_{interval}_at_{date_time_with_line}',
                image=line_img
            )
            self.post_slack_file(
                file_path=line_image_path,
                message='The last image taken')

    def save_last_drawn_image(self):
        """
        Save the last image with drawn lines of liquid levels

        :return:
        """
        date_time_with_line, line_img = self.liquid_level_monitor.get_last_line_image()
        path_to_save_image = self.all_drawn_images_folder.save_image_to_folder(
            image_name=f'{date_time_with_line}',
            image=line_img
        )
        return path_to_save_image

    def post_slack_message(self,
                           message: str):
        """
        Convenience function to send a slack message with the associated RTMControlManager. if no RTMControlManager, do nothing

        :param message:
        :return:
        """
        if self.slack_bot is None:
            return
        self.slack_bot.post_slack_message(message)

    def post_slack_file(self,
                        file_path: str,
                        message: str,
                        ):
        """
        Convenience function to post a slack file with the associated RTMControlManager. if no RTMControlManager, do nothing
        :param file_path: location of file to send
        :param message:
        :return:
        """
        if self.slack_bot is None:
            return
        self.slack_bot.post_slack_file(filepath=file_path,
                                       title=message,
                                       comment=message,
                                       )

    def create_folder_hierarchy(self):
        """
        create the folders required for the specific application
        :return:
        """
        self.slack_images_folder = self.save_folder.make_and_add_sub_folder(sub_folder_name='slack_images')
        self.all_drawn_images_folder = self.save_folder.make_and_add_sub_folder(sub_folder_name='all_drawn_images')
        self.raw_images_folder = self.save_folder.make_and_add_sub_folder('raw_images')

    def pause_the_experiment(self):
        self.pause_experiment = True

    def resume_experiment(self):
        self.pause_experiment = False

    def end_experiment(self):
        self.run_experiment = False


