import time
from typing import Union
from heinsight.vision.liquid_level import LiquidLevel, NoMeniscusFound
from heinsight.heinsight_utilities.time_manager import TimeManager
from heinsight.heinsight_utilities.try_tracker import TryTracker
from hein_utilities.slack_integration.slack_managers import RTMControlManager
from liquid_level_workflows.workflows.generic_workflow import LiquidLevelGenericWorkFlow


class FiltrationWorkflow(LiquidLevelGenericWorkFlow):
    def __init__(self,
                 liquid_level: LiquidLevel,
                 try_tracker: TryTracker,
                 time_manager: TimeManager,
                 pump,
                 number_of_monitor_liquid_level_replicate_measurements: int = 3,
                 advance_time: int = 15,
                 slack_bot: RTMControlManager = None,
                 save_folder_bool: bool = True,
                 save_folder_name: str = None,
                 save_folder_location: str = None,
                 ):
        """
        Generic workflow for vision controlled filtration; there is no implementation of how pumps are used,
        this is just the workflow logic for automated filtration


        :param LiquidLevel, liquid_level: instance of LiquidLevel, should have a TrackOneLiquidToleranceLevel tracker
        :param TryTracker, try_tracker:
        :param TimeManager, time_manager:
        :param int, advance_time: the number of seconds to allow filtration to occur before checking the liquid level
        :param RTMControlManager, slack_bot:
        :param bool, save_folder_bool: True if at the end of the application you want to save all the images
            that were taken and used throughout the application run to use
        :param str, save_folder_name: Name of the save folder - generally it would be the experiment name
        :param str, save_folder_location: location to create the folder to save everything
        """
        super().__init__(liquid_level=liquid_level,
                         try_tracker=try_tracker,
                         time_manager=time_manager,
                         do_variable_self_correction=False,
                         number_of_monitor_liquid_level_replicate_measurements
                         =number_of_monitor_liquid_level_replicate_measurements,
                         slack_bot=slack_bot,
                         save_folder_bool=save_folder_bool,
                         save_folder_name=save_folder_name,
                         save_folder_location=save_folder_location
                         )
        self.pump = pump
        self._advance_time = advance_time

    @property
    def advance_time(self) -> Union[float, int]:
        return self._advance_time

    @advance_time.setter
    def advance_time(self,
                     value: Union[float, int]):
        if isinstance(value, float) is False or isinstance(value, int) is False:
            raise TypeError('value must be of type float or int')
        self._advance_time = value

    def pre_run(self,
                image=None,
                select: bool = True,
                ):
        """
        Allow user to select the tolerance level for when self correction needs to be done, and set the liquid level
        to self correct to

        :param image:
        :param select:
        :return:
        """

        if image is None:
            image = self.liquid_level_monitor.take_picture()

        curr_time = self.datetime_manager.now_string()
        self.raw_images_folder.save_image_to_folder(image_name=curr_time,
                                                    image=image)

        if select is True:
            if self.do_variable_self_correction is True:
                self.liquid_level_monitor.liquid_level.start(
                    image=image,
                    select_region_of_interest=True,
                    set_reference=True,
                    select_tolerance=True,
                )
            else:
                self.liquid_level_monitor.liquid_level.track_liquid_tolerance_levels.reference_row=0
                self.liquid_level_monitor.liquid_level.start(
                    image=image,
                    select_region_of_interest=True,
                    set_reference=False,
                    select_tolerance=True,
                )
        else:
            self.liquid_level_monitor.liquid_level.start(
                image=image,
                select_region_of_interest=False,
                set_reference=False,
                select_tolerance=False,
            )

    def run(self,
            image=None,
            do_pre_run: bool = True,
            select=True,
            ):
        super().run(image=image,
                    do_pre_run=do_pre_run,
                    select=select)

    def advance(self):
        time.sleep(self.advance_time)

    def end_sequence(self,
                     time_since_started: float):
        """
        ending sequence of things to do. post slack messages to let user know the end time has been reached

        :param: float, time_since_started: number of hours that have elapsed since start time
        :return:
        """
        super().end_sequence(time_since_started=time_since_started)

    def monitor_liquid_level(self, list_of_percent_diff):
        return super().monitor_liquid_level(list_of_percent_diff=list_of_percent_diff)

    def not_in_tolerance(self, percent_diff):
        """
        What to do if the liquid level was not found to be within tolerance

        :param float, percent_diff: the relative distance of the current liquid level from a set reference level
        :return:
        """
        return super().not_in_tolerance(percent_diff=percent_diff)

    def self_correct(self,
                     percent_diff: float,
                     ):
        return NotImplementedError

    def check_if_should_try_again(self,
                                  no_meniscus_error: NoMeniscusFound,
                                  ):
        super().check_if_should_try_again(no_meniscus_error=no_meniscus_error)

    def try_again(self,
                  no_meniscus_error: NoMeniscusFound,
                  ):
        """
        Do what the superclass does but also save images of what the error looks like to disk
        :param no_meniscus_error:
        :return:
        """
        super().try_again(no_meniscus_error=no_meniscus_error)


class FiltrationPeristalticPump(FiltrationWorkflow):
    def __init__(self,
                 liquid_level: LiquidLevel,
                 try_tracker: TryTracker,
                 time_manager: TimeManager,
                 pump,
                 # pump: NewEraPeristalticPumpInterface,
                 do_variable_self_correction: bool,
                 number_of_monitor_liquid_level_replicate_measurements: int = 3,
                 advance_time: int = 15,
                 slack_bot: RTMControlManager = None,
                 show: bool = False,
                 save_folder_bool: bool = True,
                 save_folder_name: str = None,
                 save_folder_location: str = None,
                 time_to_self_correct: int = 5,
                 ):
        """
        Automated vision controlled filtration using a New Era pump

        :param liquid_level:
        :param try_tracker:
        :param time_manager:
        :param pump:
        :param do_variable_self_correction:
        :param slack_bot:
        :param show:
        :param save_folder_bool:
        :param save_folder_name:
        :param save_folder_location:
        :param int, time_to_self_correct: default time to self make the pump run for to self correct when not doing
            variable self correct
        """
        super().__init__(liquid_level=liquid_level,
                         try_tracker=try_tracker,
                         time_manager=time_manager,
                         pump=pump,
                         number_of_monitor_liquid_level_replicate_measurements
                         =number_of_monitor_liquid_level_replicate_measurements,
                         advance_time=advance_time,
                         slack_bot=slack_bot,
                         save_folder_bool=save_folder_bool,
                         save_folder_name=save_folder_name,
                         save_folder_location=save_folder_location
                         )
        self.time_to_self_correct = time_to_self_correct

    def set_rate(self,
                 rate: float,
                 unit=None):
        """
        Set the rate of the pump. see method in NewEraPeristalticPumpInterface

        :param rate:
        :param unit:
        :return:
        """
        self.pump.set_rate(rate=rate,
                           unit=unit

                           )

    def set_direction(self,
                      direction):
        """
        Set the direction of the pump. see method in NewEraPeristalticPumpInterface

        :param str, direction: etiher 'dispense' or 'withdray'
        :return:
        """
        self.pump.set_direction(direction=direction)

    def pre_run(self,
                image=None,
                select: bool = True,
                ):
        """
        see superclass

        :param select:
        :return:
        """
        super().pre_run(image=image, select=select)

    def run(self,
            image=None,
            do_pre_run: bool = True,
            select: bool = True,
            ):
        super().run(image=image,
                    do_pre_run=do_pre_run,
                    select=select)

    def self_correct(self,
                     percent_diff: float,
                     ):
        self.pump.pump(pump_time=self.time_to_self_correct,
                       direction='dispense',
                       wait_time=1,
                       )

        if self.liquid_level_monitor.application_liquid_level_data_save_file_path is not None:
            self.liquid_level_monitor.update_json_file_with_new_liquid_level_correction_data_values()
