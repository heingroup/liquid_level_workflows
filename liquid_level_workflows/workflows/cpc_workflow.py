import time
from datetime import datetime
from heinsight.vision.liquid_level import LiquidLevel, NoMeniscusFound
from heinsight.heinsight_utilities.time_manager import TimeManager
from heinsight.heinsight_utilities.try_tracker import TryTracker
from heinsight.heinsight_utilities.track_liquid_tolerance_levels import TrackTwoLiquidToleranceLevels
from heinsight.vision_utilities.image_analysis import ImageAnalysis
from liquid_level_workflows.workflow_utilities.liquid_level_control import calculate_variable_pump_time_new_era_peristaltic_pump
from hein_utilities.slack_integration.slack_managers import RTMControlManager
from liquid_level_workflows.workflows.generic_workflow import LiquidLevelGenericWorkFlow


class CPCWorkflow(LiquidLevelGenericWorkFlow):
    def __init__(self,
                 liquid_level: LiquidLevel,
                 try_tracker: TryTracker,
                 time_manager: TimeManager,
                 pump,
                 do_variable_self_correction: bool,
                 initial_pump_rate: float = None,
                 self_correction_pump_rate: float = None,
                 number_of_monitor_liquid_level_replicate_measurements: int = 3,
                 slack_bot: RTMControlManager = None,
                 save_folder_bool: bool = True,
                 save_folder_name: str = None,
                 save_folder_location: str = None,
                 ):
        """

        :param liquid_level:
        :param try_tracker:
        :param time_manager:
        :param pump:
        :param int, initial_pump_rate: rate for the pump to use for anything other than self correction, if there is
            an advance step that requires the pump to run. default should be to use whatever rate the pump is
            currently set at
        :param int, self_correction_pump_rate: rate for the pump to use for self correction. default is to use the
            rate the pump is currently set at
        :param do_variable_self_correction:
        :param number_of_monitor_liquid_level_replicate_measurements:
        :param slack_bot:
        :param save_folder_bool:
        :param save_folder_name:
        :param save_folder_location:
        """
        super().__init__(liquid_level=liquid_level,
                         try_tracker=try_tracker,
                         time_manager=time_manager,
                         do_variable_self_correction=do_variable_self_correction,
                         number_of_monitor_liquid_level_replicate_measurements
                         =number_of_monitor_liquid_level_replicate_measurements,
                         slack_bot=slack_bot,
                         save_folder_bool=save_folder_bool,
                         save_folder_name=save_folder_name,
                         save_folder_location=save_folder_location
                         )
        self.pump = pump
        self.initial_pump_rate = initial_pump_rate
        if self.initial_pump_rate is None:
            self.initial_pump_rate = pump.get_rate()
        self.self_correction_pump_rate = self_correction_pump_rate
        if self_correction_pump_rate is None:
            self.self_correction_pump_rate = self.initial_pump_rate

    # todo maybe move some of this up to superclass but not all
    def pre_run(self,
                image=None,
                select: bool = True,
                ):
        """
        see superclass

        :param image:
        :param select:
        :return:
        """
        # todo consider moving this up to liquid level monitor class
        # next line can throw NoMeniscusFound exception
        if image is None:
            image = self.liquid_level_monitor.take_picture()

        curr_time = self.datetime_manager.now_string()
        self.raw_images_folder.save_image_to_folder(image_name=curr_time,
                                                    image=image)

        if select is True:
            if self.do_variable_self_correction is True:
                self.liquid_level_monitor.liquid_level.start(
                    image=image,
                    select_region_of_interest=True,
                    set_reference=True,
                    select_tolerance=True,
                )
            else:
                self.liquid_level_monitor.liquid_level.track_liquid_tolerance_levels.reference_row = 0
                self.liquid_level_monitor.liquid_level.start(
                    image=image,
                    select_region_of_interest=True,
                    set_reference=True,
                    select_tolerance=True,
                )
        else:
            self.liquid_level_monitor.liquid_level.start(
                image=image,
                select_region_of_interest=False,
                set_reference=False,
                select_tolerance=False,
            )

        self.pump.set_rate(rate=self.initial_pump_rate)

        print(f'pre_run complete')

    def run(self,
            image=None,
            do_pre_run: bool = True,
            select: bool = True,
            ):
        super().run(image=image,
                    do_pre_run=do_pre_run,
                    select=select)

    def advance(self):
        """
        Does one cycle of pumping liquid pump out of the main vial and then pump back into the main vial.

        :return:
        """
        raise NotImplementedError

    def end_sequence(self,
                     time_since_started: float):
        """
        ending sequence of things to do. post slack messages to let user know the end time has been reached

        :param: float, time_since_started: number of hours that have elapsed since start time
        :return:
        """
        super().end_sequence(time_since_started=time_since_started)

    def monitor_liquid_level(self, list_of_percent_diff):
        return super().monitor_liquid_level(list_of_percent_diff=list_of_percent_diff)

    def not_in_tolerance(self, percent_diff):
        return super().not_in_tolerance(percent_diff=percent_diff)

    def self_correct(self,
                     percent_diff: float,
                     ):
        """
        Self-correct according to if there is too little/too much liquid in the vial relative to the reference line
        by pumping

        :param float, percent_diff: percentage relative to height of the image of the distance between the most
           recently measured meniscus line and the reference meniscus line
        :return:
        """
        raise NotImplementedError

    def calculate_how_much_to_self_correct(self,
                                           percent_diff: float,
                                           ):
        raise NotImplementedError

    def variable_self_correct(self,
                           percent_diff: float,
                           ):
        """
        variablely self-correct according to if there is too little/too much liquid in the vial relative to the
        reference
        line by pumping - the pumping is guided by vision. Need to pump according to the difference between the
        reference and current liquid level, but only pump 80% of what is needed to go back to the reference meniscus
        level is actually pumped; this is for just in case an incorrect meniscus was found, so that self correction is
        more robust

        :param float, percent_diff: percentage relative to height of the image of the distance between the most
           recently measured meniscus line and the reference meniscus line. if it is a negative number then the
           current liquid level is below the reference liquid level.
        :return:
        """
        raise NotImplementedError

    def pump_self_correct(self,
                          time_to_pump,
                          direction,
                          ):
        raise NotImplementedError

    def calculate_how_much_to_variable_self_correct(self,
                                                 percent_diff: float,
                                                 ):
        raise NotImplementedError

    def check_if_should_try_again(self,
                                  no_meniscus_error: NoMeniscusFound,
                                  ):
        super().check_if_should_try_again(no_meniscus_error=no_meniscus_error)

    def try_again(self,
                  no_meniscus_error: NoMeniscusFound,
                  ):
        """
        Do what the superclass does but also save images of what the error looks like to disk
        :param no_meniscus_error:
        :return:
        """
        super().try_again(no_meniscus_error=no_meniscus_error)

    def do_not_try_again(self,
                         no_meniscus_error: NoMeniscusFound,
                         ):
        """
        Do what the superclass does but also save images of what the error looks like to disk and send slack messages
        of the error images.

        :param no_meniscus_error:
        :return:
        """
        super().do_not_try_again(no_meniscus_error=no_meniscus_error)

    def do_if_a_time_interval_has_passed(self,
                                         time: datetime):
        """
        Do this if a time interval has passed based on time passed through. add the new time interval to the time
        manager, and save the latest image analyzed to the folder for slack images to be saved in. then post that
        image to slack if there is a RTMControlManager

        :param datetime, time:
        :return:
        """
        super().do_if_a_time_interval_has_passed(time=time)

    def save_last_drawn_image(self):
        """
        Save the last image with drawn lines of where the liquid levels are to the computer

        :return: str, path_to_save_image: path to the image that was saved
        """
        return super().save_last_drawn_image()

    def post_slack_message(self,
                           message: str):
        super().post_slack_message(message=message)

    def post_slack_file(self,
                        file_path: str,
                        message: str,
                        ):
        super().post_slack_file(file_path=file_path,
                                message=message)

    def create_folder_hierarchy(self):
        super().create_folder_hierarchy()


class CPCDualPumps(CPCWorkflow):
    def __init__(self,
                 liquid_level: LiquidLevel,
                 try_tracker: TryTracker,
                 time_manager: TimeManager,
                 withdraw_pump,  # pump that is set to withdraw from the vessel being watched
                 dispense_pump,  # pump that is set to dispense into the vessel being watched
                 initial_pump_rate: float,  # rate for the pump that runs constantly, in ml/min
                 do_variable_self_correction: bool,
                 time_to_self_correct: int = 10,  # number of seconds to allow only 1 pump to run for liquid level
                 # self correction in the 'non-variable' way to correct the liquid level
                 self_correction_pump_rate: float = None,
                 number_of_monitor_liquid_level_replicate_measurements: int = 3,
                 advance_time: int = 30,
                 slack_bot: RTMControlManager = None,
                 save_folder_bool: bool = True,
                 save_folder_name: str = None,
                 save_folder_location: str = None,
                 ):
        super().__init__(liquid_level=liquid_level,
                         try_tracker=try_tracker,
                         time_manager=time_manager,
                         do_variable_self_correction=do_variable_self_correction,
                         initial_pump_rate=initial_pump_rate,
                         self_correction_pump_rate=self_correction_pump_rate,
                         number_of_monitor_liquid_level_replicate_measurements
                         =number_of_monitor_liquid_level_replicate_measurements,
                         pump=withdraw_pump,
                         slack_bot=slack_bot,
                         save_folder_bool=save_folder_bool,
                         save_folder_name=save_folder_name,
                         save_folder_location=save_folder_location
                         )
        self.withdraw_pump = self.pump
        self.dispense_pump = dispense_pump
        self.initial_pump_rate = initial_pump_rate
        self.advance_time = advance_time
        self.track_liquid_tolerance_levels_fail_safe = TrackTwoLiquidToleranceLevels()

        self.time_to_self_correct = time_to_self_correct

        self.direction_to_withdraw = 'withdraw'
        self.direction_to_dispense = 'dispense'

        self.ia = ImageAnalysis()
        self.pumps_are_running = False  # for tracking if pumps are running or not, used to trigger the pumps when
        # un-pausing the experiment

    def pause_the_experiment(self):
        self.pause_experiment = True
        self.pumps_are_running = False
        self.stop_both_pumps()

    def pre_run(self,
                image=None,
                select: bool = True,
                ):
        """
        run the pre-run step to allow the user to select the reference and tolerance levels. but in a addition,
        also let the user select the fail-safe tolerance levels, where if the liquid level goes outside of the
        fail-safe tolerance levels, the the pumps are stopped and the run ends
        :param image:
        :param select:
        :return:
        """
        super().pre_run(image=image,
                        select=select)

        if image is None:
            image = self.liquid_level_monitor.liquid_level.camera.take_picture()

        curr_time = self.datetime_manager.now_string()
        self.raw_images_folder.save_image_to_folder(image_name=curr_time,
                                                    image=image)

        if select is True:
            self.track_liquid_tolerance_levels_fail_safe.select_tolerance(image=image)

    def run(self,
            image=None,
            do_pre_run: bool = True,
            select: bool = True,
            ):
        """
        The main function that puts together the methods to run liquid level monitoring of an image (that
        either must be passed through or else take a picture with the camera and use that), and then advances the
        system based on analysis of the image. What decision needs to be made based on the analysis needs to be
        separately implemented in each of the subclasses.

        The default action for the superclass run is only to delete the folder of images that were saved to
        the disk if the user doesn't want to save the images

        For this version specifically, two pumps are controlled, one will constantly run at a single rate,
        and the second will also constantly run, but it's rate may be adjusted to keep the liquid level within the
        user selected tolerance levels. If the liquid level goes outside of the fail-safe tolernace levels then both
        the pumps are stopped and the experiment ends.

        :param bool, do_pre_run: Whether to do the pre_run step or not
        :param image, an image. This will be the image that will be used to monitor the liquid level and go
        through
            the run. If none, the camera in the liquid level attribute will be used to take a photo.
        :param bool, select:
        :return:
        """

        try:
            if do_pre_run is True:  # if you do want to run the pre_run step
                # next line can throw NoMeniscusFound exception
                try:
                    self.pre_run(image=None, select=select)
                except NoMeniscusFound as error:
                    self.check_if_should_try_again(no_meniscus_error=error)

            self.liquid_level_monitor.set_up_applications_liquid_level_data_save_file()

            # the overall loop
            # initially start both the pumps
            self.start_both_pumps_at_initial_rate()
            self.pumps_are_running = True

            while self.run_experiment is True:
                if self.pause_experiment is not True:
                    # send slack message every interval of time specified in init to remind user that CPC is still
                    # running
                    curr_time = datetime.now()
                    time_since_started = self.time_manager.time_since_started(time=curr_time)

                    has_a_time_interval_elapsed = self.time_manager.has_a_time_interval_elapsed(time=curr_time)
                    if has_a_time_interval_elapsed:
                        self.do_if_a_time_interval_has_passed(time=curr_time)

                    # check if experiment has run for the amount of time user specified or not, if it has or has gone
                    # over; if gone over then end the script
                    after_end_time = self.time_manager.is_after_end_time(time=curr_time)
                    if after_end_time:
                        self.end_sequence(time_since_started=time_since_started)
                        return

                    try:
                        list_of_percent_diff = self.take_multiple_photos_and_run_liquid_level()
                        fail_safe_tolerance_bool, tolerance_bool, percent_diff = self.monitor_liquid_level(list_of_percent_diff=list_of_percent_diff)
                        if self.try_tracker.get_try_counter() is not 0:
                            self.post_slack_message('Liquid level found. Reset try counter to 0')
                            self.try_tracker.reset_try_counter()

                            date_time_with_line, line_img = self.liquid_level_monitor.liquid_level.all_images_with_lines[-1]
                            line_img = self.draw_fail_safe_tolerance_lines(image=line_img)
                            line_image_path = self.slack_images_folder.save_image_to_folder(
                                image_name=f'reset_try_counter_{date_time_with_line}',
                                image=line_img,
                            )

                    except NoMeniscusFound as error:
                        self.check_if_should_try_again(error)
                        continue

                    if fail_safe_tolerance_bool is False:  # if the liquid level was found to be outside of the fail
                        # safe tolerance levels, then stop the pumps and stop the run
                        message = 'Stopped application because liquid level went outside fail safe tolerance bounds'
                        print(message)
                        self.post_slack_message(f'{message}')

                        if self.pumps_are_running is True:
                            self.stop_both_pumps()
                        return

                    if tolerance_bool:  # if the most recent measured meniscus level found is within the tolerance
                        # bounds advance normally next line can throw NoMeniscusFound exception
                        # START THE PUMPS AGAIN IF THEY had BEEN stopped previously aka if the liquid level went out of
                        # tolerance this causes the pumps to be stopped
                        if self.pumps_are_running is False:
                            self.start_both_pumps_at_initial_rate()
                            self.pumps_are_running = True
                        try:
                            self.advance()
                        except NoMeniscusFound as error:  # if algorithm couldnt find a meniscus
                            self.check_if_should_try_again(error)
                            continue
                    else:  # most recent measured meniscus was not within tolerance bounds
                        self.not_in_tolerance(percent_diff=percent_diff)
                else:  # self.pause_experiment is True
                    time.sleep(30)
            else:  # self.run_experiment is not True
                self.post_slack_message('experiment was manually ended')
                print('experiment was manually ended')
                return

        except KeyboardInterrupt as error:
            self.post_slack_message('Stopped application script using Keyboard Interrupt')

        except Exception as error:
            print('Run has failed')
            # write the last seen image before the error image, and send that to user through slack
            self.post_slack_message(f'Something went wrong with the run. Error encountered: {error} :cry:')

            date_time_with_line, line_img = self.liquid_level_monitor.liquid_level.all_images_with_lines[-1]
            line_img = self.draw_fail_safe_tolerance_lines(image=line_img)
            date_time_for_edge, edge_img = self.liquid_level_monitor.liquid_level.all_images_edge[-1]

            line_image_path = self.slack_images_folder.save_image_to_folder(
                image_name=f'failed_exit_run_image_{date_time_with_line}',
                image=line_img
            )
            edge_image_path = self.slack_images_folder.save_image_to_folder(
                image_name=f'failed_exit_run_image_edge_{date_time_for_edge}',
                image=edge_img
            )
            self.post_slack_file(line_image_path,
                                 'Last image before error')
            self.post_slack_file(edge_image_path,
                                 'Last image before error')

            _, _, _, path_to_drawn_save_image = self.take_picture_and_run_liquid_level_algorithm()
            self.post_slack_file(path_to_drawn_save_image,
                                 'current image')

            raise error

        finally:
            if self.liquid_level_monitor.application_liquid_level_data_save_file_path is not None:
                self.liquid_level_monitor.update_json_file_with_new_liquid_level_data_values()

            try:
                self.dispense_pump.stop()
            except:
                pass
            try:
                self.withdraw_pump.stop()
            except:
                pass

            if self.save_folder_bool is False:
                self.save_folder.delete_from_disk()

    def save_last_drawn_image(self):
        """
        Save the last image with drawn lines of liquid levels

        :return:
        """
        date_time_with_line, line_img = self.liquid_level_monitor.liquid_level.all_images_with_lines[-1]
        line_img = self.draw_fail_safe_tolerance_lines(image=line_img)
        path_to_save_image = self.all_drawn_images_folder.save_image_to_folder(
            image_name=f'{date_time_with_line}',
            image=line_img
        )
        return path_to_save_image

    def start_both_pumps_at_initial_rate(self):
        self.dispense_pump.set_rate(rate=self.initial_pump_rate)
        self.withdraw_pump.set_rate(rate=self.initial_pump_rate)
        self.withdraw_pump.set_direction('withdraw')
        self.dispense_pump.set_direction('dispense')
        self.dispense_pump.run()
        self.withdraw_pump.run()

    def stop_both_pumps(self):
        self.dispense_pump.stop()
        self.withdraw_pump.stop()

    def advance(self):
        time.sleep(self.advance_time)

    def monitor_liquid_level(self, list_of_percent_diff):
        """
        Method used to monitor the liquid level. mostly similar to the superclass version. the difference here is
        that the fail safe tolerance level tracker is also used to check to see if the liquid level is within the
        tolerance levels of the fail safe tolerance tracker or not

        :return:
        """
        # instead of just taking one picture, take 3 and find the average of the tolerance_bool values and
        # round the value. Once rounded, then only take the corresponding percent_diff values that have the same
        # tolerance_bool value, where 0 is False and 1 is True, and average them to get the position of the liquid
        # level relative to how far from the reference level it is. this way, if the analysis of one image incorrectly
        # identifies the location of the liquid level compared to the other images, its value will be ignored
        number_of_times_to_try = self.number_of_monitor_liquid_level_replicate_measurements  # must be an odd number

        list_of_percent_diff = []  # list to keep track of the difference between the current liquid level height
        # with the reference height, in terms of percentage of the entire image height

        average_percent_diff = 0  # average difference between the identified current liquid level and the reference
        # liquid level, ignoring values that are outliers

        # can throw a NoMeniscusFound exception
        # implement the variance method of doing this
        for i in range(number_of_times_to_try):
            # loop through the number of times to try to take pictures to try to find the liquid level. percent_diff is
            # how far away the meniscus is from the the reference liquid level line. collect these all into
            # list_of_percent_diff
            try:
                _, _, percent_diff, _ = self.take_picture_and_run_liquid_level_algorithm()
                self.try_tracker.reset_try_counter()
                list_of_percent_diff.append(percent_diff)
            except NoMeniscusFound as error:
                self.check_if_should_try_again(no_meniscus_error=error)
                number_of_times_to_try += 1

        # use the reject_current_outliers method to remove outliers from measuring the percent_diff values
        list_of_percent_diff = self.liquid_level_monitor.reject_outliers_modified_z(list_of_percent_diff)

        # find the average percent diff from the remaining values
        for percent_diff_value in list_of_percent_diff:
            average_percent_diff += percent_diff_value
        average_percent_diff = average_percent_diff / len(list_of_percent_diff)

        # then need to convert the average percent diff back into relative to the image height value
        absolute_diff = average_percent_diff * self.liquid_level_monitor.liquid_level.track_liquid_tolerance_levels.reference_image_height
        average_liquid_level_height = self.liquid_level_monitor.liquid_level.track_liquid_tolerance_levels.get_absolute_reference_height() - absolute_diff
        average_liquid_level_percent_height = average_liquid_level_height/self.liquid_level_monitor.liquid_level.track_liquid_tolerance_levels.reference_image_height

        # on average, was the liquid level within tolerance or not, taking into account getting rid of outlier values
        average_tolerance_bool = self.liquid_level_monitor.liquid_level.in_tolerance(average_liquid_level_percent_height)
        rounded_tolerance_bool = average_tolerance_bool

        average_fail_safe_tolerance_bool = self.track_liquid_tolerance_levels_fail_safe.in_tolerance(average_liquid_level_percent_height)
        rounded_fail_safe_tolerance_bool = average_fail_safe_tolerance_bool

        # save new average liquid level percent height to memory, and after n values are in memory, save it to the
        # application liquid level data json file
        time_formatted = self.datetime_manager.now_string()

        self.application_liquid_level_data[time_formatted] = average_liquid_level_percent_height

        if self.liquid_level_monitor.application_liquid_level_data_save_file_path is not None:
            self.liquid_level_monitor.update_json_file_with_new_liquid_level_data_values()
            self.application_liquid_level_data = {}

        return rounded_fail_safe_tolerance_bool, rounded_tolerance_bool, average_percent_diff

    def take_picture_and_run_liquid_level_algorithm(self):
        """
        mostly like superclass version, except also check whether the liquid level is within the fail safe tolerance
        liquid level or not

        :return: tolerance_bool, bool: True if the liquid level is within tolerance. float, percent_diff: the
            relative distance the identified liquid level is away from the user set reference level.
            str, path_to_save_image. fail_safe_tolerance_bool, True if the liquid level is within tolerance of the
            fail safe tolerance bounds
        """
        curr_time = self.datetime_manager.now_string()
        image = self.liquid_level_monitor.liquid_level.camera.take_picture()
        self.raw_images_folder.save_image_to_folder(image_name=curr_time,
                                                    image=image)
        tolerance_bool, percent_diff = self.liquid_level_monitor.liquid_level.run(image=image)
        liquid_level_current_row = self.liquid_level_monitor.liquid_level.row
        fail_safe_tolerance_bool = self.track_liquid_tolerance_levels_fail_safe.in_tolerance(height=liquid_level_current_row)

        path_to_drawn_save_image = self.save_last_drawn_image()
        return fail_safe_tolerance_bool, tolerance_bool, percent_diff, path_to_drawn_save_image

    def not_in_tolerance(self, percent_diff):
        """
        Do this if the liquid level was found not to be within tolerance to adjust the liquid level to be within
        tolerance

        :param: float, percent_diff, the relative distance of the current liquid level from a set reference level
        :return:
        """
        if self.pumps_are_running is True:
            self.stop_both_pumps()
            self.pumps_are_running = False

        date_time_with_line, line_img = self.liquid_level_monitor.liquid_level_monitorliquid_level.all_images_with_lines[-1]
        line_img = self.draw_fail_safe_tolerance_lines(image=line_img)

        line_image_path = self.slack_images_folder.save_image_to_folder(
            image_name=f'out_of_tolerance_{date_time_with_line}',
            image=line_img
        )
        if percent_diff > 0:
            above_or_below = 'above'
        else:  # if less than tolerance then pump into the vial being watched
            above_or_below = 'below'

        print(f'liquid level {above_or_below} tolerance')
        print(f'current liquid level difference from the reference liquid level by {percent_diff}')
        self.post_slack_message(f'Liquid level moved {above_or_below} the tolerance boundaries. '
                                f'Current liquid level differs from the reference liquid level by {percent_diff}')

        # self.post_slack_file(line_image_path,
        #                      'Liquid level out of tolerance bounds image')

        # next line can throw NoMeniscusFound exception
        if self.do_variable_self_correction is True:
            self.variable_self_correct(percent_diff=percent_diff)
        else:
            self.self_correct(percent_diff=percent_diff)

        # next line can throw NoMeniscusFound exception
        try:
            # immediately analyze a photo after self correction step and send message and picture to user so they can
            # tell immediately if self correction worked or not
            _, tolerance_bool, percent_diff, path_to_drawn_save_image = \
                self.take_picture_and_run_liquid_level_algorithm()
            self.post_slack_message(f'After self correction - liquid level within tolerance bounds: {tolerance_bool}, '
                                    f'liquid level differs from the reference liquid level by {percent_diff}')
            # self.post_slack_file(path_to_drawn_save_image,
            #                      'Liquid level image after self correction')
            print(f'After self correction - liquid level within tolerance bounds: {tolerance_bool}, '
                  f'liquid level differs from the reference liquid level by {percent_diff}')
        except NoMeniscusFound as error:
            self.check_if_should_try_again(no_meniscus_error=error)

    def variable_self_correct(self,
                           percent_diff: float,
                           ):
        """
        stop one of the pumps so that self correction of the liquid level can occur; the time to self correct
        depends on the distance between the current liquid level and the reference level

        :param percent_diff:
        :return:
        """
        if percent_diff > 0:  # if more
            # than tolerance ( aka if meniscus travelled upwards)
            self_correct_direction = 'withdraw'
        else:  # if less than tolerance (aka meniscus travelled downwards)
            self_correct_direction = 'dispense'

        time_to_self_correct = self.calculate_how_much_to_variable_self_correct(percent_diff=percent_diff)

        message = f'to self correct need to {self_correct_direction}. percent diff is: {percent_diff} for ' \
                  f'{time_to_self_correct}'
        print(message)
        self.post_slack_message(message=message)
        self.pump_self_correct(time_to_pump=time_to_self_correct,
                               direction=self_correct_direction)

        if self.liquid_level_monitor.application_liquid_level_data_save_file_path is not None:
            self.liquid_level_monitor.update_json_file_with_new_liquid_level_correction_data_values()

    def self_correct(self,
                     percent_diff: float,
                     ):
        """
        stop one of the pumps so that self correction of the liquid level can occur; done with a single user set self
        correction rate and time that is not determined by the distance between the currently liquid level and the
        reference liquid level

        :param percent_diff:
        :return:
        """
        if percent_diff > 0:  # if more
            # than tolerance ( aka if meniscus travelled upwards)
            self_correct_direction = 'withdraw'
        else:  # if less than tolerance (aka meniscus travelled downwards)
            self_correct_direction = 'dispense'

        message = f'to self correct need to {self_correct_direction}. percent diff is: {percent_diff} for ' \
                  f'{self.time_to_self_correct}'
        print(message)
        self.post_slack_message(message=message)

        self.pump_self_correct(time_to_pump=self.time_to_self_correct,
                               direction=self_correct_direction
                               )
        if self.liquid_level_monitor.application_liquid_level_data_save_file_path is not None:
            self.liquid_level_monitor.update_json_file_with_new_liquid_level_correction_data_values()

    def pump_self_correct(self,
                          time_to_pump,
                          direction,
                          ):
        if direction == 'withdraw':
            self_correction_pump = self.withdraw_pump
        else:  # is self_correct_direction is 'dispense'
            self_correction_pump = self.dispense_pump

        self_correction_pump.set_rate(rate=self.self_correction_pump_rate)
        self_correction_pump.start()

        time.sleep(time_to_pump)

        self_correction_pump.stop()

    def calculate_how_much_to_variable_self_correct(self,
                                                 percent_diff: float,
                                                 ):

        """
        right now this is just copy and paste of the same method from the automatedcpcneweraperistalticpump class

        :param float, percent_diff: percentage relative to height of the image of the distance between the most
           recently measured meniscus line and the reference meniscus line
        :return:
        """
        # need to remember that liquid level gives the ratio in terms of pixels, but for this,the percent_diff is
        # given as a float that is a percentage of the image total height. Since the same width and height of the
        # image should be used when comparing the current image to the first image that liquid level's
        # find_pump_to_pixel_ratio must have been run on, just need to convert between pixels and relative height of
        # the image

        absolute_value_of_percent_diff = abs(percent_diff) * 0.8  # need to use the absolute value when finding how many
        # pixels to correct by, since the percent_diff value can be a negative number if the change in meniscus
        # height is below the reference meniscus

        # to convert the relative percent_diff to number of pixels, need to multiply image height (from liquid level
        # instance) by the percent_diff
        image_height, image_width, _ = self.liquid_level_monitor.liquid_level.loaded_image.shape

        pixels_need_to_self_correct_by = int(image_height * absolute_value_of_percent_diff)

        time_to_self_correct, _ = \
            calculate_variable_pump_time_new_era_peristaltic_pump(
                liquid_level=self.liquid_level_monitor.liquid_level,
                pixels_need_to_self_correct_by=pixels_need_to_self_correct_by)

        return time_to_self_correct

    def draw_fail_safe_tolerance_lines(self, image):
        """
        Draw two lines at the location of the two user selected fail safe tolerance lines. this is nearly identical
        to the draw_tolerance_lines() method in the liquid_level class
        :param image:
        :return:
        """
        _, img_width = self.ia.find_image_height_width(image=image)

        list_of_absolute_tolerance_levels = self.track_liquid_tolerance_levels_fail_safe.get_absolute_tolerance_height()

        if len(list_of_absolute_tolerance_levels) is 0:
            return image

        for absolute_tolerance_level in list_of_absolute_tolerance_levels:
            tolerance_left_point = (0, absolute_tolerance_level)
            tolerance_right_point = (img_width, absolute_tolerance_level)

            # draw blue line for tolerance
            pink = (88, 100, 300)
            text_position = (0, 60)
            image = self.ia.draw_line(image=image,
                                      left_point=tolerance_left_point,
                                      right_point=tolerance_right_point,
                                      colour=pink,
                                      text='fail-safe tolerance',
                                      text_position=text_position
                                      )
        return image


class CPCSinglePump(CPCWorkflow):
    def __init__(self,
                 liquid_level: LiquidLevel,
                 try_tracker: TryTracker,
                 time_manager: TimeManager,
                 pump,
                 do_variable_self_correction: bool,
                 initial_pump_rate: float = None,
                 self_correction_pump_rate: float = None,
                 time_to_self_correct: int = 10,
                 number_of_monitor_liquid_level_replicate_measurements: int = 3,
                 advance_time: int = 15,
                 slack_bot: RTMControlManager = None,
                 save_folder_bool: bool = True,
                 save_folder_location: str = None,
                 save_folder_name: str = None,
                 wait_time: int = 1,
                 ):
        """
      Uses a New Era peristaltic pump

        :param liquid_level:
        :param try_tracker:
        :param time_manager:
        :param pump: For the pump, the set direction of the pump should be so that liquid
            will be pumped out of the vial that is being watched by the camera
        :param do_variable_self_correction:
        :param slack_bot:
        :param show:
        :param save_folder_bool:
        :param save_folder_location:
        :param save_folder_name:
        :param wait_time:
        """
        super().__init__(liquid_level=liquid_level,
                         try_tracker=try_tracker,
                         time_manager=time_manager,
                         pump=pump,
                         do_variable_self_correction=do_variable_self_correction,
                         initial_pump_rate=initial_pump_rate,
                         self_correction_pump_rate=self_correction_pump_rate,
                         number_of_monitor_liquid_level_replicate_measurements
                         =number_of_monitor_liquid_level_replicate_measurements,
                         slack_bot=slack_bot,
                         save_folder_bool=save_folder_bool,
                         save_folder_location=save_folder_location,
                         save_folder_name=save_folder_name,
                         )
        self.advance_time = advance_time
        self.wait_time = wait_time  # how long to wait between consecutive pumps from the same pump
        self.time_to_pump_correction = time_to_self_correct  # hard coded in non-variable self correction value
        self.direction_to_withdraw = 'withdraw'
        self.direction_to_dispense = 'dispense'

    def pre_run(self,
                image=None,
                select: bool = True,
                ):
        super().pre_run(image=image,
                        select=select,
                        )

    def run(self,
            image=None,
            do_pre_run: bool = True,
            select: bool = True,
            ):
        super().run(image=image,
                    do_pre_run=do_pre_run,
                    select=select)

    def advance(self):
        print(f'start a cycle')

        self.pump.pump(
            pump_time=self.advance_time,
            direction=self.direction_to_dispense,
            wait_time=self.wait_time
        )

        time.sleep(2)

        self.pump.pump(
            pump_time=self.advance_time,
            direction=self.direction_to_withdraw,
            wait_time=self.wait_time
        )

        time.sleep(2)

        print(f'finished a cycle')

    def self_correct(self,
                     percent_diff: float,
                     ):
        self_correct_time = self.calculate_how_much_to_self_correct(percent_diff=percent_diff)

        if percent_diff > 0:  # if more than tolerance then pump out of the vial being watched
            self_correct_direction = self.direction_to_withdraw
        else:  # if less than tolerance then pump into the vial being watched
            self_correct_direction = self.direction_to_dispense

        print(f'direction to pump: {self_correct_direction}, '
              f'time to pump to self correct: f{self_correct_time}')
        self.post_slack_message(f'direction to pump: {self_correct_direction}, '
                                f'time to pump to self correct: {self.time_to_pump_correction}')

        self.pump_self_correct(time_to_pump=self_correct_time,
                               direction=self_correct_direction,
                               )

        if self.liquid_level_monitor.application_liquid_level_data_save_file_path is not None:
            self.liquid_level_monitor.update_json_file_with_new_liquid_level_correction_data_values()

    def calculate_how_much_to_self_correct(self,
                                           percent_diff: float,
                                           ):
        return self.time_to_pump_correction

    def variable_self_correct(self,
                           percent_diff: float,
                           ):
        """

        :param float percent_diff:  if it is a negative number then the
           current liquid level is below the reference liquid level.
        :return:
        """
        time_to_self_correct = self.calculate_how_much_to_variable_self_correct(percent_diff=percent_diff)

        if percent_diff > 0:  # if more
            # than tolerance ( aka if meniscus travelled upwards)
            self_correct_direction = self.direction_to_withdraw
            print(f'percent diff: {percent_diff}, so need to pump out of vial')

        else:  # if less than tolerance (aka meniscus travelled downwards)
            self_correct_direction = self.direction_to_dispense
            print(f'percent diff: {percent_diff}, so need to pump in to vial')

        if self_correct_direction is self.direction_to_withdraw:
            self_correction_direction_string = 'out of vial'
        else:
            self_correction_direction_string = 'in to vial'

        print(f'variable self correct: direction to pump: {self_correction_direction_string}, time to pump to self '
              f'correct: {time_to_self_correct}')
        self.post_slack_message(f'variable self correct: direction to pump: {self_correction_direction_string}, '
                                f'time to pump to self correct: {time_to_self_correct}')

        self.pump_self_correct(time_to_pump=time_to_self_correct,
                               direction=self_correct_direction,
                               )

    def pump_self_correct(self,
                          time_to_pump,
                          direction,
                          ):
        self.pump.set_rate(rate=self.self_correction_pump_rate)
        self.pump.pump(pump_time=time_to_pump,
                       direction=direction,
                       wait_time=self.wait_time,
                       )
        self.pump.set_rate(rate=self.self_correction_pump_rate)

    def calculate_how_much_to_variable_self_correct(self,
                                                 percent_diff: float,
                                                 ):

        """
        Find out the time required in order to move the liquid level so that it moves to be 80% closer to the
        reference level

        # Calls the calculate_variable_pump_time_new_era_peristaltic_pump function from the liquid level package to
        calculate the time
        # required to
        # self correct the current liquid level to the reference liquid level, and find the rate to spin the
        # peristaltic
        # pump to achieve this

        :param float, percent_diff: percentage relative to height of the image of the distance between the most
           recently measured meniscus line and the reference meniscus line
        :return:
        """
        # need to remember that liquid level gives the ratio in terms of pixels, but for this,the percent_diff is
        # given as a float that is a percentage of the image total height. Since the same width and height of the
        # image should be used when comparing the current image to the first image that liquid level's
        # find_pump_to_pixel_ratio must have been run on, just need to convert between pixels and relative height of
        # the image

        absolute_value_of_percent_diff = abs(percent_diff) * 0.8  # need to use the absolute value when finding how many
        # pixels to correct by, since the percent_diff value can be a negative number if the change in meniscus
        # height is below the reference meniscus

        # to convert the relative percent_diff to number of pixels, need to multiply image height (from liquid level
        # instance) by the percent_diff
        image_height, image_width, _ = self.liquid_level_monitor.liquid_level.loaded_image.shape

        pixels_need_to_self_correct_by = int(image_height * absolute_value_of_percent_diff)

        time_to_self_correct, rpm_for_pump_to_pixel_ratio = \
            calculate_variable_pump_time_new_era_peristaltic_pump(
                liquid_level=self.liquid_level_monitor.liquid_level,
                pixels_need_to_self_correct_by=pixels_need_to_self_correct_by)

        return time_to_self_correct
