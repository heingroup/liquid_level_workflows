"""
Collection of methods that require the use of liquid level sensing and pump control. There are 2 versions of most of
the methods provided here. One version for a North Robotics peristaltic pump; the code to control that is located in the
gronckle repository in north_robotics_hardware.peristaltic_pump_control.py. Another version for  a new era
peristaltic pump, and in this case then the control code for that is in the newera package

These methods are used to manipulate a liquid level

"""

import cv2
from heinsight.vision.liquid_level import LiquidLevel
from heinsight.vision_utilities.image_analysis import ImageAnalysis

_ia = ImageAnalysis()

"""
Code linked for controlling a liquid level using a New Era peristaltic pump
"""


def click_and_pump_new_era(ne_pump,
                           liquid_level: LiquidLevel,
                           image=None,
                           ):
    image_height, image_width, _ = image.shape
    move_to_selection_points = []

    print(f'the pump to pixel ratio is {liquid_level.pump_to_pixel_ratio}')
    # take picture with camera if one is not passed
    if image is None:
        image = liquid_level.camera.take_picture()
    # find current liquid level
    liquid_level.load_and_find_level(img=image)
    initial_absolute_liquid_level_height = int(liquid_level.row * image_height)
    print(f"initial liquid level is at {initial_absolute_liquid_level_height} pixel height")

    # todo display picture with camera with line of where current level is and let user select where the meniscus
    # should move to

    def make_selection(event, x, y, flags, param):

        # if left mouse button clicked, record the starting(x, y) coordinates
        if event is cv2.EVENT_LBUTTONDOWN:
            move_to_selection_points.append((x, y))

        if len(move_to_selection_points) is 1:
            # draw line for where to move liquid level to
            move_to_left_point = (0, move_to_selection_points[0][1])
            move_to_right_point = (image_width, move_to_selection_points[0][1])
            cv2.line(image, move_to_left_point, move_to_right_point, (0, 255, 0), 2)
            cv2.imshow('Select level to move to ', image)

    # clone image and set up cv2 window
    image = image.copy()
    clone = image.copy()
    cv2.namedWindow('Select level to move to')
    cv2.setMouseCallback('Select level to move to', make_selection)

    # keep looping until 'q' is pressed - here the user can click on the image to draw a line of where to move the
    # liquid level to; must only click on the image once. press 'c' button to select it as the position to move the
    # liquid level to. press 'r' to reset and give them another chance to select where to move th liquid level to
    while True:
        # display image, wait for a keypress
        cv2.imshow('Select level to move to', image)
        key = cv2.waitKey(1) & 0xFF

        # if 'r' key is pressed, reset the cropping region
        if key == ord('r'):
            move_to_selection_points = []
            image = clone.copy()

        # if 'c' key pressed break from while True loop
        elif key == ord('c'):
            break

    # if there was a single point selected from the image to move the liquid level to, then calculate the time
    # required to rotate the peristaltic pump in order to move the current liquid level to that level, and do it
    if len(move_to_selection_points) == 1:
        cv2.namedWindow('Selected level to move liquid to')
        cv2.imshow('Selected level to move liquid to', image)
        cv2.waitKey(0)

        # adjust liquid level based on user input on image to move the meniscus there
        level_to_move_to = move_to_selection_points[0][1]  # pixel height in the image to move to
        print(f"pixel height in the image to move to: {level_to_move_to}")
        # normalize the height of the level to move to by the image height
        level_to_move_to = level_to_move_to/image_height
        print(f"fraction of image to move liquid level by: {level_to_move_to}")

        relative_pixels_need_to_self_correct_by = abs(liquid_level.row - level_to_move_to)  # relative as both these
        # values are nomalized by the image height

        # then can calculate the absolute number of pixels in order to self correct
        pixels_need_to_self_correct_by = int(relative_pixels_need_to_self_correct_by * image_height)

        if (liquid_level.row - level_to_move_to) < 0:
            # if the current liquid level is above where you want to move to
            current_level_is_above_target = True
        else:
            current_level_is_above_target = False

        if current_level_is_above_target:
            direction_to_move = 'withdraw'  # set direction to move liquid out of the container
        else:  # current level is below where you want to move
            direction_to_move = 'dispense'  # set direction to move liquid into the container
        print(f'pixels_need_to_self_correct_by: {pixels_need_to_self_correct_by}')
        time_to_move, rpm_for_pump_to_pixel_ratio = \
            calculate_variable_pump_time_new_era_peristaltic_pump(liquid_level=liquid_level,
                                                               pixels_need_to_self_correct_by=pixels_need_to_self_correct_by)
        print(f'time to move: {time_to_move}')
        print(f'move in direction {direction_to_move}')

        ne_pump.pump(direction=direction_to_move,
                     pump_time=time_to_move,
                     rpm=rpm_for_pump_to_pixel_ratio,
                     )
    # todo make a gui specific version of click and pump so that the live stream can play while this occurs that doesnt
    # have the cv2.waitKey() calls so the user can also see what happens after/where the liquid level is after the
    # pumping finishes
    image_after_movement = liquid_level.camera.take_picture()
    # find current liquid level
    liquid_level.load_and_find_level(image_after_movement)
    final_image_with_liquid_level_line_drawn = liquid_level.draw_menisci(img=image_after_movement)
    cv2.imshow("image after move liquid", final_image_with_liquid_level_line_drawn)
    absolute_pixel_height = int(liquid_level.row * image_height)
    print(f"current liquid level after movement is at {absolute_pixel_height} pixel height")
    cv2.waitKeyEx(0)
    cv2.destroyAllWindows()


def find_pump_to_pixel_ratio_new_era(liquid_level,
                                     pump,  # instance of a new era pump
                                     time_to_pump=None,  # time to pump in seconds; applies only for peristaltic pumps
                                     pump_rate=None,  # rate to use
                                     ):
    # take a photo, find where the liquid level is at. this will have to be done before the actual peristaltic
    # loop run so this function will use liquid_level.start to create a reference image and get the user to select a ROI

    if pump_rate is None:
        pump_rate = pump.get_rate()

    pump.set_rate(rate=pump_rate)

    liquid_level.start(select_region_of_interest=True, set_reference=False, select_tolerance=False)
    # take a photo, find where the liquid level is; this should be different from the initial
    initial_image = liquid_level.camera.take_picture()
    liquid_level.load_and_find_level(initial_image)
    image_height, image_width, _ = initial_image.shape
    initial_image_copy = initial_image.copy()
    initial_image_with_liquid_line_drawn = liquid_level.draw_menisci(img=initial_image_copy)
    print('press "enter" on the image window to continue')
    _ia.display_image(image_name="initial image before first pump", image=initial_image_with_liquid_line_drawn)
    # next line can throw NoMeniscusFound exception
    initial_image_meniscus_row = liquid_level.row  # liquid_level.row now should be the row where the current meniscus
    #  is
    # found at
    print(f'before first pump, liquid level is at {initial_image_meniscus_row} pixel height')

    pump.pump(pump_time=time_to_pump,
              direction='withdraw',
              )

    print(f'finish first pump')
    # take a photo, find where the liquid level is; this should be different from the initial
    final_image = liquid_level.camera.take_picture()
    liquid_level.load_and_find_level(final_image)
    final_image_copy = final_image.copy()
    final_image_with_liquid_line_drawn = liquid_level.draw_menisci(img=final_image_copy)
    print('press "enter" on the image window to continue')
    _ia.display_image(image_name="image after first pump", image=final_image_with_liquid_line_drawn)
    # next line can throw NoMeniscusFound exception
    final_image_meniscus_row = liquid_level.row  # liquid_level.row now should be the row where the current meniscus is
    # found at
    print(f'after first pump, liquid level is at {final_image_meniscus_row} pixel height')

    # find the difference between the two liquid levels in terms of pixels
    pixel_difference = final_image_meniscus_row - initial_image_meniscus_row  # this should be a positive number since liquid
    # should have been pumped out of the container, and so meniscus travels down
    # note also that pixel difference should be a float as it is normalized by the image height
    # so need to convert that to absolute pixels
    pixel_difference = int(image_height * pixel_difference)
    print(f'pixel height travelled: {pixel_difference}')

    # update the pump_to_pixel_ratio dictionary
    liquid_level.pump_to_pixel_ratio['time'] = time_to_pump
    liquid_level.pump_to_pixel_ratio['rate'] = pump_rate
    liquid_level.pump_to_pixel_ratio['pixels'] = pixel_difference

    print(f'the pump to pixel ratio is {liquid_level.pump_to_pixel_ratio}')


def calculate_variable_pump_time_new_era_peristaltic_pump(liquid_level, pixels_need_to_self_correct_by):
    # calculation for pump based on an image to move the liquid level
    # need to find out the time needed for the specific rate to correct for the identified number of pixels
    rate_for_pump_to_pixel_ratio = liquid_level.pump_to_pixel_ratio['rate']
    time_for_pump_to_pixel_ratio = liquid_level.pump_to_pixel_ratio['time']
    pixels_for_pump_to_pixel_ratio = liquid_level.pump_to_pixel_ratio['pixels']

    # to know the time required to do the self correction step is then just a matter of doing:
    # time_to_self_correct = pixels_need_to_self_correct_by * 1/pixels_for_pump_to_pixel_ratio *
    # time_for_pump_to_pixel_ratio
    # then just need to pump at the rate_for_pump_to_pixel_ratio for time_to_self_correct seconds
    # also to make things simple/work, convert the time from a potential float value into an int
    time_to_self_correct = int(pixels_need_to_self_correct_by * (
            time_for_pump_to_pixel_ratio / pixels_for_pump_to_pixel_ratio))

    return time_to_self_correct, rate_for_pump_to_pixel_ratio


"""
Code linked for controlling North Robotics peristaltic pump -  the pump class is located in the 
            gronckle repository in north_robotics_hardware.peristaltic_pump_control.py
"""


# assume liquid level already has the pump to pixel ratio
# todo need to fix this up, put more output images and better labelling to be able to see the difference and the changes
# todo need to test to makes sure this wasnt broken in the refactoring
def click_and_pump(peristaltic_pump_control,
                   liquid_level: LiquidLevel,
                   image=None,
                   ):
    image_height, image_width, _ = image.shape
    move_to_selection_points = []

    print(f'the pump to pixel ratio is {liquid_level.pump_to_pixel_ratio}')
    # take picture with camera if one is not passed
    if image is None:
        image = liquid_level.camera.take_picture()
    # find current liquid level
    liquid_level.load_and_find_level(img=image)
    initial_absolute_liquid_level_height = int(liquid_level.row * image_height)
    print(f"initial liquid level is at {initial_absolute_liquid_level_height} pixel height")

    # todo display picture with camera with line of where current level is and let user select where the meniscus
    # should move to

    def make_selection(event, x, y, flags, param):

        # if left mouse button clicked, record the starting(x, y) coordinates
        if event is cv2.EVENT_LBUTTONDOWN:
            move_to_selection_points.append((x, y))

        if len(move_to_selection_points) is 1:
            # draw line for where to move liquid level to
            move_to_left_point = (0, move_to_selection_points[0][1])
            move_to_right_point = (image_width, move_to_selection_points[0][1])
            cv2.line(image, move_to_left_point, move_to_right_point, (0, 255, 0), 2)
            cv2.imshow('Select level to move to ', image)

    # clone image and set up cv2 window
    image = image.copy()
    clone = image.copy()
    cv2.namedWindow('Select level to move to')
    cv2.setMouseCallback('Select level to move to', make_selection)

    # keep looping until 'q' is pressed - here the user can click on the image to draw a line of where to move the
    # liquid level to; must only click on the image once. press 'c' button to select it as the position to move the
    # liquid level to. press 'r' to reset and give them another chance to select where to move th liquid level to
    while True:
        # display image, wait for a keypress
        cv2.imshow('Select level to move to', image)
        key = cv2.waitKey(1) & 0xFF

        # if 'r' key is pressed, reset the cropping region
        if key == ord('r'):
            move_to_selection_points = []
            image = clone.copy()

        # if 'c' key pressed break from while True loop
        elif key == ord('c'):
            break

    # if there was a single point selected from the image to move the liquid level to, then calculate the time
    # required to rotate the peristaltic pump in order to move the current liquid level to that level, and do it
    if len(move_to_selection_points) == 1:
        cv2.namedWindow('Selected level to move liquid to')
        cv2.imshow('Selected level to move liquid to', image)
        cv2.waitKey(0)

        # adjust liquid level based on user input on image to move the meniscus there
        level_to_move_to = move_to_selection_points[0][1]  # pixel height in the image to move to
        print(f"pixel height in the image to move to: {level_to_move_to}")
        # normalize the height of the level to move to by the image height
        level_to_move_to = level_to_move_to/image_height
        print(f"fraction of image to move liquid level by: {level_to_move_to}")

        relative_pixels_need_to_self_correct_by = abs(liquid_level.row - level_to_move_to)  # relative as both these
        # values are nomalized by the image height

        # then can calculate the absolute number of pixels in order to self correct
        pixels_need_to_self_correct_by = int(relative_pixels_need_to_self_correct_by * image_height)

        if (liquid_level.row - level_to_move_to) < 0:
            # if the current liquid level is above where you want to move to
            current_level_is_above_target = True
        else:
            current_level_is_above_target = False

        if current_level_is_above_target:
            direction_to_move = 1  # set direction to move liquid out of the container
        else:  # current level is below where you want to move
            direction_to_move = -1  # set direction to move liquid into the container
        print(f'pixels_need_to_self_correct_by: {pixels_need_to_self_correct_by}')
        time_to_move, rpm_for_pump_to_pixel_ratio = \
            calculate_variable_pump_time_north_robotics_peristaltic_pump(liquid_level=liquid_level,
                                                                      pixels_need_to_self_correct_by=pixels_need_to_self_correct_by)
        print(f'time to move: {time_to_move}')
        print(f'move in direction {direction_to_move}')
        peristaltic_pump_control.spin_joint(direction=direction_to_move,
                                            time=time_to_move,
                                            rpm=rpm_for_pump_to_pixel_ratio,
                                            )
    # todo make a gui specific version of click and pump so that the live stream can play while this occurs that doesnt
    # have the cv2.waitKey() calls so the user can also see what happens after/where the liquid level is after the
    # pumping finishes
    image_after_movement = liquid_level.camera.take_picture()
    # find current liquid level
    liquid_level.load_and_find_level(image_after_movement)
    final_image_with_liquid_level_line_drawn = liquid_level.draw_menisci(img=image_after_movement)
    cv2.imshow("image after move liquid", final_image_with_liquid_level_line_drawn)
    absolute_pixel_height = int(liquid_level.row * image_height)
    print(f"current liquid level after movement is at {absolute_pixel_height} pixel height")
    cv2.waitKeyEx(0)
    cv2.destroyAllWindows()


# todo should find a better place to put these next two functions in; it goes beyond just what the liquid level
# class should do
# todo this current version only works for the North Robotics peristaltic pump but there needs to be a version
# for a New era peristaltic pump too
def find_pump_to_pixel_ratio(liquid_level,
                             pump,  # instance of a pump
                             direction,  # either -1 or 1 for peristaltic, or 0 or 1 for syringe pump. this
                             # is the direction to move liquid out of the liquid being watched (technically
                             # direction is called state for a cavro pump)
                             time_to_pump=None,  # time to pump in seconds; applies only for peristaltic pumps
                             rpm=None,  # rpm to use to pump; applies only for peristaltic pumps
                             volume=None,  # volume to pump;  applies only to syringe pumps
                             flowrate_pull=None,  # flowrate to pump;  applies only to syringe pumps
                             ):
    # the numbers in should come from after you have found the dead volume for the specfic pump and set up.

    # if using a peristaltic pump, need to put something in for time_to_pump, rpm, and direction when initializing.
    # if using a syringe pump, need to put something in for direction, when initializing.
    # function to automate 'calibration' of a pump. This means create a relation between a pump (rpm and time if it
    # is a peristaltic pump, or volume for a syringe pump) and how far (pixels in an image)
    # pump can either be a type of syringe pump or peristaltic pump (peristaltic pump is controlled by the
    # PeristalticPumpControl class, currently in Gronckle)
    # flowrate_pull is just used so user can specify a slow enough rate to prevent caviation when pulling liquid
    # for this, the liquid in the container needs to be within view of the camera; somewhere in the middle is best

    # todo rename variables in here; the second image should not be called final
    # take a photo, find where the liquid level is at. this will have to be done before the actual peristaltic
    # loop run so this function will use liquid_level.start to create a reference image and get the user to select a ROI

    liquid_level.start(select_region_of_interest=True, set_reference=True, select_tolerance=False)
    # take a photo, find where the liquid level is; this should be different from the initial
    initial_image = liquid_level.camera.take_picture()
    liquid_level.load_and_find_level(initial_image)
    image_height, image_width, _ = initial_image.shape
    initial_image_copy = initial_image.copy()
    initial_image_with_liquid_line_drawn = liquid_level.draw_menisci(img=initial_image_copy)
    cv2.imshow("initial image before first pump", initial_image_with_liquid_line_drawn)
    # next line can throw NoMeniscusFound exception
    initial_image_meniscus_row = liquid_level.row  # liquid_level.row now should be the row where the current meniscus
    #  is
    # found at
    print(f'before first pump, liquid level is at {initial_image_meniscus_row} pixel height')

    # pump according to the time and rpm OR volume, in the correct direction; this should be so that vol that
    # gets pumped > dead volume
    if time_to_pump is not None and rpm is not None:  # is using peristaltic pump
        pump.spin_joint(time=time_to_pump,
                        rpm=rpm,
                        direction=direction)
    else:  # if using syringe pump
        pump.set_state(direction)
        pump.start(  # start pump
            volume=volume,  # for target volume
            direction='pull',  # in the pull direction
            flowrate=flowrate_pull,
        )
        direction = 1 - direction  # invert and change state
        pump.set_state(direction, 1.)  # invert state
        pump.empty()  # empty the syringe
    print(f'finish first pump')
    # take a photo, find where the liquid level is; this should be different from the initial
    final_image = liquid_level.camera.take_picture()
    liquid_level.load_and_find_level(final_image)
    final_image_copy = final_image.copy()
    final_image_with_liquid_line_drawn = liquid_level.draw_menisci(img=final_image_copy)
    cv2.imshow("image after first pump", final_image_with_liquid_line_drawn)
    # next line can throw NoMeniscusFound exception
    final_image_meniscus_row = liquid_level.row  # liquid_level.row now should be the row where the current meniscus is
    # found at
    print(f'after first pump, liquid level is at {final_image_meniscus_row} pixel height')

    # find the difference between the two liquid levels in terms of pixels
    pixel_difference = final_image_meniscus_row - initial_image_meniscus_row  # this should be a positive number since liquid
    # should have been pumped out of the container, and so meniscus travels down
    # note also that pixel difference should be a float as it is normalized by the image height
    # so need to convert that to absolute pixels
    pixel_difference = int(image_height * pixel_difference)
    print(f'pixel height travelled: {pixel_difference}')

    print(f'return liquid moved back to original vial')
    # update the pump_to_pixel_ratio dictionary appropriately depending on the type of pump that was used
    if time_to_pump is not None and rpm is not None:  # is using peristaltic pump
        liquid_level.pump_to_pixel_ratio['time'] = time_to_pump
        liquid_level.pump_to_pixel_ratio['rpm'] = rpm
        liquid_level.pump_to_pixel_ratio['pixels'] = pixel_difference

    else:  # if using syringe pump
        liquid_level.pump_to_pixel_ratio['volume'] = volume
        liquid_level.pump_to_pixel_ratio['pixels'] = pixel_difference

    # pump at the same time and rpm OR volume in the opposite direction to return the liquid level back to
    # the reference location
    if time_to_pump is not None and rpm is not None:  # is using peristaltic pump
        reverse_direction = -1 * direction
        pump.spin_joint(time=time_to_pump,
                        rpm=rpm,
                        direction=reverse_direction)
    else:  # if using syringe pump
        reverse_direction = 1 - direction  # find the reverse of the first direction
        pump.set_state(reverse_direction)
        pump.start(  # start pump
            volume=volume,  # for target volume
            direction='pull',  # in the pull direction
            flowrate=flowrate_pull,
        )
        reverse_direction = 1 - reverse_direction  # invert and change state
        pump.set_state(reverse_direction, 1.)  # invert state
        pump.empty()  # empty the syringe

    image_after_second_pump = liquid_level.camera.take_picture()
    liquid_level.load_and_find_level(image_after_second_pump)
    image_after_second_pump_copy = image_after_second_pump.copy()
    image_after_second_pump_with_liquid_line_drawn = liquid_level.draw_menisci(img=image_after_second_pump_copy)
    cv2.imshow("image after second pump", image_after_second_pump_with_liquid_line_drawn)
    # next line can throw NoMeniscusFound exception
    after_second_pump_meniscus_row = liquid_level.row  # liquid_level.row now should be the row where the current
    # meniscus is
    # found at
    print(f'after second pump, liquid level is at {after_second_pump_meniscus_row} pixel height')
    print(f'pixel height travelled: {after_second_pump_meniscus_row - final_image_meniscus_row}')
    print(f'difference between intial and final pixel height row: '
          f'{initial_image_meniscus_row - after_second_pump_meniscus_row}')
    print(f'the pump to pixel ratio is {liquid_level.pump_to_pixel_ratio}')
    cv2.waitKeyEx(0)
    cv2.destroyAllWindows()


# todo move this to the liquid control collection of methods script instead, and rename this so it is
# specifically for peristaltic pump
def calculate_variable_pump_time_north_robotics_peristaltic_pump(liquid_level, pixels_need_to_self_correct_by):
    # todo right now this only works for if liquid level is for peristaltic pump - need to make a separate
    # version that works for the cavro pump

    # calculation for pump based on an image to move the liquid level
    # need to find out the time needed for the specific rpm to correct for the identified number of pixels
    rpm_for_pump_to_pixel_ratio = liquid_level.pump_to_pixel_ratio['rpm']
    time_for_pump_to_pixel_ratio = liquid_level.pump_to_pixel_ratio['time']
    pixels_for_pump_to_pixel_ratio = liquid_level.pump_to_pixel_ratio['pixels']

    # to know the time required to do the self correction step is then just a matter of doing:
    # time_to_self_correct = pixels_need_to_self_correct_by * 1/pixels_for_pump_to_pixel_ratio *
    # time_for_pump_to_pixel_ratio
    # then just need to pump at the rpm_for_pump_to_pixel_ratio for time_to_self_correct seconds
    # also to make things simple/work, convert the time from a potential float value into an int
    time_to_self_correct = int(pixels_need_to_self_correct_by * (
            time_for_pump_to_pixel_ratio / pixels_for_pump_to_pixel_ratio))

    # todo need to make a syringe pump version here

    return time_to_self_correct, rpm_for_pump_to_pixel_ratio



